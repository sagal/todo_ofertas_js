import { FTPClient } from "https://deno.land/x/ftpc@v1.3.0/mod.ts";
import { S3Bucket } from "https://deno.land/x/s3@0.4.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const partial = Deno.env.get("PARTIAL");
const integratorApiUrl = Deno.env.get("INTEGRATOR_API");

const ftpHost = Deno.env.get("FTP_HOST");
const user = Deno.env.get("FTP_USER");
const pass = Deno.env.get("FTP_PASS");
const fileFolderPath = Deno.env.get("FILE_FOLDER_PATH");

const awsKeyId = Deno.env.get("AWS_ACCESS_KEY_ID");
const awsSecretKey = Deno.env.get("AWS_SECRET_ACCESS_KEY");
const awsRegion = Deno.env.get("AWS_REGION");

const bucket = new S3Bucket({
  accessKeyID: awsKeyId,
  secretKey: awsSecretKey,
  bucket: "sagal-excel-uploads-0181239103940129",
  region: awsRegion,
});

const delay = (ms) => new Promise((res) => setTimeout(res, ms));

async function init() {
  let credentials = {
    user: user,
    pass: pass,
    mode: "passive",
    port: 21,
  };
  const client = new FTPClient(ftpHost, credentials);

  let allArticleSkus = await getExistingArticles();
  for (let articleSku of allArticleSkus) {
    if (articleSku) {
      try {
        await processAllImagesForArticle(articleSku, client);
      } catch (e) {
        console.log(
          `Unable to process images for article ${articleSku}: ${e.message}`
        );
      }
    }
  }
}

async function getExistingArticles() {
  let articleSkus = [];
  for (let ecommerceId of Object.values(clientEcommerce)) {
    let response = await fetch(
      `${integratorApiUrl}/api/client/${clientId}/ecommerce/${ecommerceId}/products`
    );
    let allArticleSkusAsKeys = await response.json();

    for (let articleSku of Object.keys(allArticleSkusAsKeys)) {
      if (!articleSkus.find((x) => x == articleSku)) {
        articleSkus.push(articleSku);
      }
    }
  }

  return articleSkus;
}

async function processAllImagesForArticle(articleSku, client) {
  let connected = false;
  let timeouts = 0;
  let data = [];
  let processedImages = [];
  while (!connected && timeouts <= 10) {
    try {
      await client.connect();
      console.log("Client connected");
      data = await client.list(`${fileFolderPath}`);
      data = data
        .filter(
          (x) =>
            x.replace("/imgs/", "").split("_")[0].trim() ===
            articleSku.toString()
        )
        .sort();
      connected = true;
      await client.close();
    } catch (e) {
      console.log(
        `Failed to connect to server : ${JSON.stringify(
          e
        )}. Reconnecting... Attempt ${timeouts + 1}`
      );
      await delay(1000);
      timeouts++;
    }
  }
  if (timeouts == 11) {
    throw new Error(`Failed to connect to server: too many timeouts`);
  }

  for (let file of data) {
    let maxConnect = 0;
    let finished = false;
    while (!finished && maxConnect <= 10) {
      try {
        await client.connect();
        console.log("Client connected");
        let imageFile = await client.download(file);
        await client.close();
        finished = true;
        let processedImage = await processImageFile(
          file.replace("/imgs/", "").trim(),
          imageFile
        );
        processedImages.push(processedImage);
      } catch (e) {
        console.log(
          `Failed to download file ${file}: ${JSON.stringify(
            e
          )}. Reconnecting... Attempt ${maxConnect + 1}`
        );
        await delay(1000);
        maxConnect++;
      }
    }
    if (maxConnect == 11) {
      console.log(`Unable to retrieve file: ${file}`);
    }
  }

  await sendImagesToSagal(articleSku, processedImages);
}

async function processImageFile(imageName, imageFile) {
  console.log(`Uploading image ${imageName}...`);

  let imageUrl = await uploadImageAndGetURI(imageName, imageFile);

  return imageUrl;
}

async function uploadImageAndGetURI(imageName, imageData) {
  try {
    await bucket.putObject(
      `/todoOfertas_test/${imageName.toLowerCase()}`,
      imageData,
      {
        contentType: "image/jpeg",
        acl: "public-read",
      }
    );
    console.log(
      `Image uploaded: https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/todoOfertas_test/${imageName.toLowerCase()}`
    );
    return `https://sagal-excel-uploads-0181239103940129.s3.amazonaws.com/todoOfertas_test/${imageName.toLowerCase()}`;
  } catch (e) {
    console.log(`Unable to upload image: ${e.message}`);
  }
}

async function sendImagesToSagal(articleSku, articleImages) {
  try {
    let processedArticle = {
      sku: articleSku,
      client_id: clientId,
      options: {
        merge: false,
        partial: partial === "true",
      },
      integration_id: clientIntegrationId,
      ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
        let ecommerceProps = {
          ecommerce_id: ecommerce_id,
          options: {
            override_create: {
              send: false,
            },
          },
          properties: [
            {
              images: articleImages,
            },
          ],
          variants: [],
        };
        return ecommerceProps;
      }),
    };
    await sagalDispatch(processedArticle);
  } catch (e) {
    console.log(`Unable to process images for article: ${articleSku}`);
  }
}
await init();
