import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const partial = Deno.env.get("PARTIAL");
const renameFile = Deno.env.get("RENAME_FILE");
const processedFilesDirectory = Deno.env.get("FTP_PROCESSED_DIRECTORY");

const ftpHost = Deno.env.get("FTP_HOST");
const user = Deno.env.get("FTP_USER");
const pass = Deno.env.get("FTP_PASS");
const fileFolderPath = Deno.env.get("FILE_FOLDER_PATH");

let articleMap = {};

async function init() {
  let credentials = {
    user: user,
    pass: pass,
    mode: "passive",
    port: 21,
  };
  const client = new FTPClient(ftpHost, credentials);

  try {
    await client.connect();
    console.log("Client connected");
    await processAllStockFiles(client);
    await client.close();
  } catch (e) {
    console.log(`Unable to process stock files: ${e.message}`);
  }

  let articles = await processArticleMap(articleMap);

  await sagalDispatchBatch({
    products: articles,
    batch_size: 100,
  });
}

async function processAllStockFiles(client) {
  let data = await client.list(`./${fileFolderPath}`);

  console.log(data);
  console.log("Sorted");
  console.log(data.sort());

  for (let file of data.sort()) {
    try {
      let filename = file.split("/").slice(-1);
      let stockFile = await client.download(file);

      await processStockFile(stockFile);

      if (renameFile === "true") {
        console.log("Moving to processed...");
        await client.rename(
          file,
          `${processedFilesDirectory}/${filename}`,
          stockFile
        );
        console.log("Done");
      }
    } catch (e) {
      console.log(`Unable to process file ${file}: ${e.message}`);
    }
  }
}

async function processStockFile(stockFile) {
  const decoder = new TextDecoder();
  let stockData = decoder.decode(stockFile).split("\n");

  for (let articleStockData of stockData) {
    let article = articleStockData;
    if (article && article != "") {
      try {
        let articleToClean = article.split("|");
        let articleFields = [];

        for (let cleanedArticle of articleToClean) {
          articleFields.push(cleanedArticle.replace(/"/g, ""));
        }

        let articleSku =
          articleFields[13] === "0" || articleFields[13] === "1"
            ? articleFields[0].trim()
            : articleFields[13].trim();
        let articleName = articleFields[4].trim();
        let articleDescription = articleFields[5];
        let articlePrice = {
          currency: articleFields[3] == "0" ? "$" : "U$S",
          value: Number(articleFields[2].replace(/,/g, ".")),
        };
        let isActive = Number(articleFields[15]);
        let variantStock = Number(articleFields[1]);
        let variantSku = articleFields[0].trim();
        let variantColor = articleFields[11].trim();
        let variantSize = articleFields[10].trim();
        let articleWarranty =
          articleFields[14] != "0" && articleFields[14] != "1"
            ? articleFields[14]
            : articleFields[12];
        let articleCategory = articleFields[9].trim();
        let articleBrand = articleFields[6];
        let articleMaterial = articleFields[22];
        let isVariant = false;

        let articleAttributes = {};

        if (articleWarranty) {
          articleAttributes.GARANTIA = articleWarranty;
        }

        if (articleBrand) {
          articleAttributes.MARCA = articleBrand;
        }

        if (articleMaterial) {
          articleAttributes.MATERIAL = articleMaterial;
        }

        let variantAttributes = {};

        if (variantSize) {
          isVariant = true;
          variantAttributes.SIZE = variantSize;
        }

        if (variantColor) {
          isVariant = true;
          variantAttributes.COLOR = variantColor;
        }

        if (!articleMap[articleSku]) {
          articleMap[articleSku] = {
            sku: articleSku.trim(),
          };
        }

        articleMap[articleSku].name = articleName;
        articleMap[articleSku].description = articleDescription;
        articleMap[articleSku].price = articlePrice;
        articleMap[articleSku].category = articleCategory;
        articleMap[articleSku].attributes = articleAttributes;

        if (!articleMap[articleSku].hasVariants) {
          articleMap[articleSku].hasVariants = false;
        }

        if (isVariant) {
          articleMap[articleSku].hasVariants = true;
          if (!articleMap[articleSku].variants) {
            articleMap[articleSku].variants = {};
          }
          articleMap[articleSku].variants[variantSku] = {};
          articleMap[articleSku].variants[variantSku].sku = variantSku.trim();
          articleMap[articleSku].variants[variantSku].attributes =
            variantAttributes;
          articleMap[articleSku].variants[variantSku].stock = variantStock;
          articleMap[articleSku].variants[variantSku].isActive = isActive;
        } else {
          articleMap[articleSku].stock = variantStock;
          articleMap[articleSku].isActive = isActive;
        }
      } catch (e) {
        console.log(`Error parsing article ${article}: ${e.message}`);
      }
    }
  }
}

async function processArticleMap(articleMap) {
  let processedArticles = [];
  for (let article of Object.values(articleMap)) {
    let processedArticle = {
      sku: article.sku,
      client_id: clientId,
      options: {
        merge: false,
        partial: partial === "true",
      },
      integration_id: clientIntegrationId,
      ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
        let ecommerceProps = {
          ecommerce_id: ecommerce_id,
          properties: [],
          variants: [],
        };
        return ecommerceProps;
      }),
    };

    for (let ecommerce of processedArticle.ecommerce) {
      ecommerce.properties.push({
        name: article.name,
      });
      ecommerce.properties.push({
        description: article.description,
      });
      ecommerce.properties.push({
        price: article.price,
      });
      if (article.category) {
        ecommerce.properties.push({
          categories: {
            [article.category]: article.category,
          },
        });
      }
      ecommerce.properties.push({
        attributes: article.attributes,
      });
      if (article.hasVariants) {
        ecommerce.variants = await processAllVariantsForArticle(article);
      } else {
        ecommerce.properties.push({ stock: Number(article.stock) });
        ecommerce.properties.push({
          metadata: {
            isActive: `${article.isActive}`,
          },
        });
      }
    }
    processedArticles.push(processedArticle);
  }
  return processedArticles;
}

async function processAllVariantsForArticle(article) {
  let processedVariants = [];
  for (let variant of Object.values(article.variants)) {
    let processedVariant = {
      sku: variant.sku,
      properties: [
        {
          attributes: variant.attributes,
        },
        { stock: Number(variant.stock) },
        {
          metadata: {
            isActive: `${variant.isActive}`,
          },
        },
      ],
    };
    processedVariants.push(processedVariant);
  }
  return processedVariants;
}

async function processStockFileObsolete(stockFile) {
  const decoder = new TextDecoder();
  let stockData = decoder.decode(stockFile).split("\n");

  for (let articleStockData of stockData) {
    let article = articleStockData;
    if (article && article != "") {
      try {
        let articleToClean = article.split("|");
        let articleFields = [];

        for (let cleanedArticle of articleToClean) {
          articleFields.push(cleanedArticle.replace(/"/g, ""));
        }

        let articleSku =
          articleFields[13] === "0" || articleFields[13] === "1"
            ? articleFields[0].trim()
            : articleFields[13].trim();
        let articleName = articleFields[4].trim();
        let articleDescription = articleFields[5];
        let articlePrice = {
          currency: articleFields[3] == "0" ? "$" : "U$S",
          value: Number(articleFields[2].replace(/,/g, ".")),
        };
        let isActive = Number(articleFields[15]);
        let variantStock = Number(articleFields[1]);
        let variantSku = articleFields[0].trim();
        let variantColor = articleFields[11].trim();
        let variantSize = articleFields[10].trim();
        let articleWarranty =
          articleFields[14] != "0" && articleFields[14] != "1"
            ? articleFields[14]
            : articleFields[12];
        let articleCategory = articleFields[9].trim();
        let articleBrand = articleFields[6];
        let articleMaterial = articleFields[22];
        let isVariant = false;

        let articleAttributes = {};

        if (articleWarranty) {
          articleAttributes.GARANTIA = articleWarranty;
        }

        if (articleBrand) {
          articleAttributes.MARCA = articleBrand;
        }

        if (articleMaterial) {
          articleAttributes.MATERIAL = articleMaterial;
        }

        let variantAttributes = {};

        if (variantSize) {
          isVariant = true;
          variantAttributes.SIZE = variantSize;
        }

        if (variantColor) {
          isVariant = true;
          variantAttributes.COLOR = variantColor;
        }

        if (!articleMap[articleSku]) {
          articleMap[articleSku] = {
            sku: articleSku.trim(),
            client_id: clientId,
            options: {
              merge: false,
              partial: partial === "true",
            },
            integration_id: clientIntegrationId,
            ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
              let ecommerceProps = {
                ecommerce_id: ecommerce_id,
                properties: [],
                variants: [],
              };
              return ecommerceProps;
            }),
          };
        }

        for (let ecommerce of articleMap[articleSku].ecommerce) {
          ecommerce.properties.push({
            name: articleName,
          });
          ecommerce.properties.push({
            description: articleDescription,
          });
          ecommerce.properties.push({
            price: articlePrice,
          });
          if (articleCategory) {
            ecommerce.properties.push({
              categories: {
                [articleCategory]: articleCategory,
              },
            });
          }
          ecommerce.properties.push({
            attributes: articleAttributes,
          });
          if (isVariant) {
            ecommerce.variants.push({
              sku: variantSku.trim(),
              properties: [
                {
                  attributes: variantAttributes,
                },
                { stock: Number(variantStock) },
                {
                  metadata: {
                    isActive: `${isActive}`,
                  },
                },
              ],
            });
          } else {
            ecommerce.properties.push({ stock: Number(variantStock) });
            ecommerce.properties.push({
              metadata: {
                isActive: `${isActive}`,
              },
            });
          }
        }
      } catch (e) {
        console.log(`Error parsing article: ${e.message}`);
      }
    }
  }
}

await init();
